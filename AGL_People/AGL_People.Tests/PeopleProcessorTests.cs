﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AGL_People.Model;
using System.Collections.Generic;
using System.Linq;

namespace AGL_People.Tests
{
    [TestClass]
    public class PeopleProcessorTests
    {
        [TestMethod]
        public void Test_MixedGenderCatsAndNoCats_Success()
        {
            var people = new List<PeopleModel>() { new PeopleModel()
                {
                    name = "Kathy",gender="Female",age=24,
                    pets = new List<PetModel>() {
                    new PetModel {name="AX",type="Cat" },new PetModel {name="AXZ",type="Dog" }
                   }
                },
                new PeopleModel()
                {
                    name = "George",gender="Male",age=29,
                    pets = new List<PetModel>() {
                    new PetModel {name="CD",type="Cat" }
                   }
                },
                new PeopleModel()
                {
                    name = "Sarah",gender="Female",age=24,
                    pets = new List<PetModel>() {
                    new PetModel {name="ZZ",type="Cat" },new PetModel {name="AX",type="Dog" }
                   }
                },
                new PeopleModel()
                {
                    name = "NoPets",gender="Female",age=24,
                    pets = null
                },
            };

            var result = new PeopleProcessor().ProcessPeopleData(people);

            Assert.IsTrue(result.Count == 2);
            var maleGenderItem = result.Single(s => s.Gender == "Male");
            Assert.IsTrue(maleGenderItem.CatNames[0]=="CD");
            var femaleGenderItem = result.Single(s => s.Gender == "Female");
            Assert.IsTrue(femaleGenderItem.CatNames.Count==2);
            Assert.IsTrue(femaleGenderItem.CatNames[0]=="AX");
            Assert.IsTrue(femaleGenderItem.CatNames[1] == "ZZ");
        }
        [TestMethod]
        public void Test_OwnerMultiCats_Success()
        {
            var people = new List<PeopleModel>() { new PeopleModel()
                {
                    name = "Tony",gender="Male",age=2,
                    pets = new List<PetModel>() {
                    new PetModel {name="AX",type="Cat" },new PetModel {name="Furry",type="Cat" }
                   }
                },
                new PeopleModel()
                {
                    name = "Sarah",gender="Female",age=2,
                    pets = new List<PetModel>() {
                    new PetModel {name="CD",type="Cat" }
                   }
                }
            };

            var result = new PeopleProcessor().ProcessPeopleData(people);

            Assert.IsTrue(result.Count == 2);
            var maleGenderItem = result.Single(s => s.Gender == "Male");
            Assert.IsTrue(maleGenderItem.CatNames[0] == "AX");
            Assert.IsTrue(maleGenderItem.CatNames[1] == "Furry");
            var femaleGenderItem = result.Single(s => s.Gender == "Female");
            Assert.IsTrue(femaleGenderItem.CatNames[0] == "CD");
        }
        [TestMethod]
        public void Test_MaleOnlyOwner_Success()
        {
            var people = new List<PeopleModel>() { new PeopleModel()
                {
                    name = "Tony",gender="Male",age=2,
                    pets = new List<PetModel>() {
                    new PetModel {name="AX",type="Cat" },new PetModel {name="AX",type="Dog" }
                   }
                },
                new PeopleModel()
                {
                    name = "George",gender="Male",age=2,
                    pets = new List<PetModel>() {
                    new PetModel {name="CD",type="Cat" }
                   }
                }
            };

            var result = new PeopleProcessor().ProcessPeopleData(people);

            Assert.IsTrue(result.Count == 1);
            Assert.IsTrue(result[0].Gender=="Male");
            Assert.IsTrue(result[0].CatNames[0]== "AX");
            Assert.IsTrue(result[0].CatNames[1] == "CD");
        }
        [TestMethod]
        public void Test_OwnersNoCats_Success()
        {
            var people = new List<PeopleModel>() { new PeopleModel()
                {
                    name = "Tony",gender="Male",age=2,
                    pets = new List<PetModel>() {
                    new PetModel {name="AX",type="Mouse" },new PetModel {name="AX",type="Dog" }
                   }
                },
                new PeopleModel()
                {
                    name = "George",gender="Female",age=2,
                    pets = new List<PetModel>() {
                    new PetModel {name="CD",type="Lion" }
                   }
                }
            };

            var result = new PeopleProcessor().ProcessPeopleData(people);

            Assert.IsTrue(result.Count == 0);

        }
    }
}
