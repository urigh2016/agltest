﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGL_People
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var peopleDataProvider = new PeopleDataProvider();
                var list = peopleDataProvider.GetPeople("http://agl-developer-test.azurewebsites.net/people.json").Result;
                Console.WriteLine("-----------------------------------------");
                var genderAndCats = new PeopleProcessor().ProcessPeopleData(list);
                foreach (var gender in genderAndCats)
                {
                    Console.WriteLine(gender.Gender);
                    foreach (var catName in gender.CatNames)
                    {
                        Console.WriteLine('\t' + catName);
                    }
                }
                Console.WriteLine("-----------------------------------------");
                Console.WriteLine("Press any button to exit");
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error occured ", ex.ToString());
            }
        }
    }
}
