﻿using AGL_People.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGL_People
{
    public class PeopleProcessor
    {
        /// <summary>
        /// output a list of all the cats in alphabetical order under a heading of the gender of their owner.
        /// </summary>
        /// <param name="peopleSource">People and pets data set</param>
        /// <returns></returns>
        public List<GenderAndCatsModel> ProcessPeopleData(IList<PeopleModel> peopleSource)
        {
            var genderAndCats = from genederAndPets in
                                    (
                                    from p in peopleSource
                                    where p.pets!=null
                                    from pet in p.pets
                                    select new { Gender = p.gender, PetName = pet.name, PetType = pet.type }
                                    )
                                where genederAndPets.PetType == "Cat"
                                select new { Gender = genederAndPets.Gender, CatName = genederAndPets.PetName };

            return ( from gndcat in genderAndCats
                    group gndcat.CatName by gndcat.Gender into g
                    select new GenderAndCatsModel()
                    {
                        Gender = g.Key,
                        CatNames = g.ToList().OrderBy((catName) => catName).ToList()
                    }).ToList();

        }
    }
}
