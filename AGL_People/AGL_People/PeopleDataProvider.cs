﻿using AGL_People.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AGL_People
{
    public class PeopleDataProvider
    {
        /// <summary>
        /// Download and parse people list
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public async Task<IList<PeopleModel>> GetPeople(string filePath)
        {
            HttpClient client = new HttpClient();
            var fileData = await client.GetAsync(filePath);
            var peopleJson = await fileData.Content.ReadAsStringAsync();
            return  JsonConvert.DeserializeObject<IList<PeopleModel>>(peopleJson);
        }
    }
}
