﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGL_People.Model
{
    public class PeopleModel
    {
        public string name {get;set;}
        public string gender { get; set; }
        public IList<PetModel> pets { get; set; }
        public int age { get; set; }
    }
    public class PetModel
    {
        public string name { get; set; }
        public string type { get; set; }
    }
}
