﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGL_People.Model
{
    public class GenderAndCatsModel
    {
        public string Gender { get; set; }

        public List<string> CatNames { get; set; }
    }
}
